################################################################################
## Calibration arguments
################################################################################

calibration-model=None
spline-calibration-envelope-dict=None
spline-calibration-nodes=5
spline-calibration-amplitude-uncertainty-dict=None
spline-calibration-phase-uncertainty-dict=None

################################################################################
## Data generation arguments
################################################################################

ignore-gwpy-data-quality-check=True
gps-tuple=None
gps-file=None
timeslide-file=None
timeslide-dict=None
trigger-time=None
gaussian-noise=True
n-simulation=1
data-dict=None
data-format=None
channel-dict=None

################################################################################
## Detector arguments
################################################################################

coherence-test=False
detectors=['H1', 'L1']
duration=4.0
generation-seed=101
psd-dict=None
psd-fractional-overlap=0.5
post-trigger-duration=2.0
sampling-frequency=4096
psd-length=32
psd-maximum-duration=1024
psd-method=median
psd-start-time=None
maximum-frequency=None
minimum-frequency=20
zero-noise=False
tukey-roll-off=0.4
resampling-method=lal

################################################################################
## Injection arguments
################################################################################

injection=True
injection-dict={'chirp_mass': 40.051544979894693, 'mass_ratio': 0.9183945489993522, 'a_1': 0., 'a_2': 0., 'tilt_1': 0., 'tilt_2': 0., 'phi_12': 0., 'phi_jl': 0., 'luminosity_distance': 1000., 'dec': 0.2205292600865073, 'ra': 3.952677097361719, 'theta_jn': 1.8795187965094322, 'psi': 2.6973435044499543, 'phase': 3.686990398567503, 'geocent_time': 0.040833669551002205}
injection-file=None
injection-numbers=None
injection-waveform-approximant=None

################################################################################
## Likelihood arguments
################################################################################

distance-marginalization=True
distance-marginalization-lookup-table=/bmk/bilby/data/TD.npz
phase-marginalization=False
time-marginalization=True
jitter-time=True
reference-frame=H1L1
time-reference=geocent
likelihood-type=GravitationalWaveTransient
roq-folder=None
roq-weights=None
roq-scale-factor=1
extra-likelihood-kwargs=None

################################################################################
## Output arguments
################################################################################

create-plots=False
plot-calibration=False
plot-corner=False
plot-marginal=False
plot-skymap=False
plot-waveform=False
plot-format=png
create-summary=False
email=None
notification=Never
existing-dir=None
webdir=None
summarypages-arguments=None

################################################################################
## Prior arguments
################################################################################

default-prior=BBHPriorDict
deltaT=0.2
prior-file=None
prior-dict={chirp-mass: Uniform(name='chirp_mass', minimum=35, maximum=45, unit='$M_{\odot}$'), mass_ratio:Uniform(name='mass_ratio', minimum=0.125, maximum=1), a_1:Uniform(name='a_1', minimum=0, maximum=0.99), a_2:Uniform(name='a_2', minimum=0, maximum=0.99), tilt_1:Sine(name='tilt_1'), tilt_2:Sine(name='tilt_2'), phi_12:Uniform(name='phi_12', minimum=0, maximum=2 * np.pi, boundary='periodic'), phi_jl:Uniform(name='phi_jl', minimum=0, maximum=2 * np.pi, boundary='periodic'), luminosity_distance:PowerLaw(alpha=2, name='luminosity_distance', minimum=1e2, maximum=5e3), dec:Cosine(name='dec'), ra:Uniform(name='ra', minimum=0, maximum=2 * np.pi, boundary='periodic'), theta_jn:Sine(name='theta_jn'), psi:Uniform(name='psi', minimum=0, maximum=np.pi, boundary='periodic'), phase:Uniform(name='phase', minimum=0, maximum=2 * np.pi, boundary='periodic')}
convert-to-flat-in-component-mass=False

################################################################################
## Post processing arguments
################################################################################

postprocessing-executable=None
postprocessing-arguments=None
single-postprocessing-executable=None
single-postprocessing-arguments=None

################################################################################
## Sampler arguments
################################################################################

sampler=dynesty
sampling-seed=101
n-parallel=1
sampler-kwargs={'nact': 10, 'nlive': 250, 'dlogz': 30, 'first_update': {'min_eff': 20}, 'npool': 4, 'check_point': False, 'maxiter': 10}

################################################################################
## Waveform arguments
################################################################################

waveform-generator=bilby.gw.waveform_generator.WaveformGenerator
reference-frequency=20.0
waveform-approximant=IMRPhenomXPHM
catch-waveform-errors=True
pn-spin-order=-1
pn-tidal-order=-1
pn-phase-order=-1
pn-amplitude-order=0
numerical-relativity-file=None
waveform-arguments-dict=None
mode-array=[None]
frequency-domain-source-model=lal_binary_black_hole

