### Overview

This repository provides a benchmarking suite for [bilby](https://git.ligo.org/lscsoft/bilby).
With thanks to Greg Ashton and Colm Talbot, it will run the `bilby_pipe_analysis` stage
of a workflow, configured to look for an injection with an artificially high
`dlogz= 20` so that the benchmark completes in a reasonable time, while also
having run long enough that the sampling performance should be meaningful as
a figure of merit. The specific figure of merit for this benchmark is the
number of likelihood evaluations per second. It runs with the
[dynesty](https://github.com/joshspeagle/dynesty) sampler and at present uses
the `IMRPhenomXPHM` waveform.

The benchmark relies on the [numa_benchmarks](https://git.ligo.org/joshua.willis/numa_benchmarks)
Python package to manage its processes. This package queries the machine
on which it runs to determine how many cpus are present (note that
`lscpu` uses the term 'cpu' for what many might term a 'core'; `numa_benchmarks`
tries to follow the `lscpu` terminology since that is what it uses
internally to query the machine) and on what NUMA nodes they are layed out.

This particular benchmark uses four threads. It will start one job for every
four cpus detected, and will set the CPU affinity mask of each benchmark job
to only the four cpus it is allocated (note that each job will print out its
CPU affinity mask in its combined `stdout/stderr` file in its working directory).
By default, the orchestrator will not split those four cpus across
NUMA nodes, and will enforce that each job only allocate its memory from
the NUMA node on which it runs.  If benchmarking on a machine where the
number of cpus in each NUMA node is not divisible by four, this will be
impossible and so the default configuration will fail.  In such a case, when
you invoke the benchmark via the docker command line as below, you should be
sure you add `-n -s` at the very end. If the total number of cpus on the machine
is not divisble by four, then in addition to these two options you must
also add `-u`, or the orchestrator will fail because it detects that the
machine is underutilized. Note that when you add `-u` all that happens is that
the benchmark is allowed to run withoug fully loading the machine; it does
not start some additional process, and so the performance could be artificially
high compared to a real-life scenario where some other compute-heavy processes
might use the remaining cores.

### Running the benchmark

The benchmark is designed to be run from the CI built
docker containers. First, the system has to be prepared,
and then the correct container run.

#### Prerequisites

For this benchmark, the only prerequisite for the machine
is that it have docker installed and an internet connection
so that it can download the image. It has been tested with
Docker 20.10.12.  All of the data the benchmark needs will
be present in the container image itself; there is no need
to make external data available to the container, just the
approriate working directory as noted below.

#### Running the container

Once a suitable version of docker is installed, you may run
the benchmark as follows:

1. Change to a working directory where you will run the
benchmark. The container will create files here, so
you need to have write access.

1. Create a script named `run_bilby_bmk.sh` with the following contents:

   ```
   #!/bin/bash
   BENCHMARK_IMAGE="containers.ligo.org/joshua.willis/bilby_benchmark:latest"
   
   docker run --cap-add SYS_NICE -v $PWD:/results -i ${BENCHMARK_IMAGE}
   ```

   Note that the `--cap-add SYS_NICE` is necessary to ensure that the scripts
   inside the container can set the memory policy and cpu affinity as they
   require; the benchmark will fail if the container is not started with
   this capability.

1. Make the script executable:

    ```
    chmod +x run_bilby_bmk.sh
    ```

1. Now you can run it and redirect its `stdout` and `stderr`:

   ```
   nohup ./run_bilby_bmk.sh </dev/null 1>out.txt 2>&1 &
   ```

1. Alternately, instead of creating the wrapper script and running it in
the background, you could invoke docker with `-d` instead of the `-i` in
the script above, and then follow its progress with `docker logs`. In that
case it is probably convenient to give the container a name, and more work
would likely be required to capture the container `stdout/stderr` after
it exits.

1. As noted above in the overview, if the number of cpus per
NUMA node is not divisible by four, you will want to add `-s -n`
to the very end of the `docker run` command in your script.  If the
total number of cpus on the entire machine is also not
divisible by four, then you will also need to add `-u`, and
should be aware that the benchmark will be unable to fully
saturate the machine.

### Benchmark results

When the container exits, if it has done so
successfully, you will have in your run directory
a new directory hierarchy of the form:
```
bilby_benchmark_<date>_<time>/benchmark
```

Inside the `benchmark` directory will be a file named
`bilby_benchmark_results.json`. This is the main
result file, and contains the figure of merit from each
benchmark job. For the Bilby benchmark, that figure of
merit is `likelihood_evals_per_second`. The `.json`
results file should be formatted to that it can be easily
read into a `pandas` dataframe, and from there exported
to your format of choice. You may also use it to calculate
whatever summary statistics you deem useful, depending
on what application you have for your benchmark.

As an example, if you have a python ecosystem where both
`pandas` and `openpyxl` are installed, then you should be
able to parse these results with the following code:
```
import pandas as pd

df = pd.read_json("bilby_benchmark_<date>_<time>/benchmark/bilby_benchmark_results.json”)
with pd.ExcelWriter(‘my_benchmark_results.xlsx') as writer:
    df.to_excel(writer, sheet_name="Bilby results", index=False)
```

Under the `benchmark` directory will also be
subdirectories for each job that ran, where they will
have written their individual outputs into yet a further
subdirectory. The job working directories will also
contain for each job its combined `stdout` and `stderr`.
This file might be useful for debugging, or also to confirm
that the CPU affinity was set as expected, since each job
should be configured to report that.
