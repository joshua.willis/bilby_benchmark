FROM igwn/base:el8

COPY auxfiles/conda.repo /etc/yum.repos.d/conda.repo

RUN rpm --import https://repo.anaconda.com/pkgs/misc/gpgkeys/anaconda.asc
RUN dnf -y install conda which numactl git && dnf clean all

RUN mkdir -p /bmk/bilby/data
COPY auxfiles/bilby_benchmark /etc/conda/bilby_benchmark
COPY auxfiles/config_complete.ini /bmk/bilby/data/config_complete.ini
COPY auxfiles/test_config.ini /bmk/bilby/data/test_config.ini
COPY scripts/* /bmk/bilby/bin/
RUN curl -SL "https://ldas-jobs.ligo.caltech.edu/~joshua.willis/benchmarking_support/bilby/TD.npz" --output /bmk/bilby/data/TD.npz
RUN curl -SL "https://ldas-jobs.ligo.caltech.edu/~joshua.willis/benchmarking_support/bilby/data_generation.pickle" --output /bmk/bilby/data/data_generation.pickle

ARG CONDAPATH=/opt/conda

RUN ${CONDAPATH}/bin/conda create -y -n bilby_benchmark --file /etc/conda/bilby_benchmark && \
    ${CONDAPATH}/bin/conda clean --all --force-pkgs-dirs --yes && \
    mkdir -p ${CONDAPATH}/pkgs/ && \
    touch ${CONDAPATH}/pkgs/urls.txt

RUN ${CONDAPATH}/envs/bilby_benchmark/bin/python -m pip install git+https://git.ligo.org/joshua.willis/bilby.git@test_mp_fix_v1.1.5

ENV PATH /bmk/bilby/bin:$PATH
ENV LAL_DEBUG_LEVEL 0
WORKDIR /results
ENTRYPOINT ["/bmk/bilby/bin/bilby_benchmark_orchestrator.sh"]
