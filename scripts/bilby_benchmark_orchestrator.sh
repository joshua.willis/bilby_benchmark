#!/bin/bash
#
# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

# Exit on error:
set -e

CMDLINE=`getopt -o htans --long help,test,allow-underutilize,non-local-mem,split-nodes -n 'bilby_benchmark_orchestrator' -- "$@"`

if [ $? != 0 ] ; then echo "Exiting" >&2 ; exit 1 ; fi

eval set -- "$CMDLINE"

testmode=0
allow_underuse=0
non_local=0
split_nodes=0

while true; do
    case "$1" in
	-t | --test ) testmode=1; shift ;; 
	-a | --allow-underutilize ) allow_underuse=1 ; shift ;;
	-n | --non-local-mem ) non_local=1 ; shift ;;
	-s | --split-nodes ) split_nodes=1 ; shift ;;
        -h | --help )
	    echo "Run the bilby benchmark on the current machine, fully"
	    echo "loading each cpu on the machine, and then extract the"
	    echo "likelihood-evaluations-per-second figure of merit from"
	    echo "each such run."
	    echo
	    echo "The orchestrator first creates and changes into a"
	    echo "directory 'bilby_benchmark_<date>_<time>."
	    echo "Within this, it creates a sub-directory names"
	    echo "'benchmark', under which each benchmark job will create"
	    echo "its own working directory and write its own output. It"
	    echo "is in the benchmark directory that the overall results"
	    echo "will be stored."
	    echo
	    echo "Note well: each job in this benchmark requires four cpus"
	    echo "(each job is multi-threaded). By default, this benchmark"
	    echo "will only assign four cpus to each job in such a way that"
	    echo "jobs are not split across NUMA nodes, and so that every"
	    echo "cpu is assigned to exactly one job. If it cannot do these"
	    echo "things it will fail. In that case, if you still want to"
	    echo "run the benchmark, you will need to give some set of the"
	    echo "-a, -s, and -n options described below. Note that it does"
	    echo "not make sense to give -s without also giving -n (though"
	    echo "the converse is certainly possible) but this orchestrator"
	    echo "script will not check for that"
	    echo
	    echo "Usage: bilby_benchmark_orchestrator.sh <options>"
	    echo
	    echo "Optional arguments:"
	    echo "  -t | --test                Enable test mode, which uses stand-in"
	    echo "                             executables that do no real work."
            echo "  -a | --allow-underutilize  Allow some cpus to be unassigned to"
	    echo "                             any jobs."
	    echo "  -n | --non-local-mem       Allow jobs bound by their cpu to one"
	    echo "                             NUMA memory node to allocate memory"
	    echo "                             from any NUMA node."
	    echo "  -s | --split-nodes         Allow multi-threaded jobs to be"
	    echo "                             assigned to cpus from more than one"
	    echo "                             NUMA node."
	    echo "  -h | --help         Print this help message and exit"
	    exit 0 ;;
	--) shift ; break ;;
        * ) echo "Error: unrecognized argument" ; exit 1 ;;
    esac
done

startdir="${PWD}"
basedir="${PWD}/bilby_benchmark_$(date +%Y%m%d_%H_%M_%S)"
echo "$(date +%c): Creating top-level directory ${basedir}"
mkdir -p $basedir; cd $basedir

source /opt/conda/etc/profile.d/conda.sh

if [[ $testmode -ne 0 ]] ; then
    echo "$(date +%c): Running in test mode"
    istest=1
    verbose="--verbose"
fi

if [[ $allow_underuse -eq 0 ]] ; then
    underutilize="--forbid-underutilize"
fi

if [[ $non_local -eq 0 ]] ; then
    localmem="--only-local-mem"
fi

if [[ $split_nodes -ne 0 ]] ; then
    splitnodes="--allow-split-nodes"
fi

benchmark_driver=`which run_one_benchmark.sh`
results_parser=`which parser_driver.sh`

conda_env="bilby_benchmark"
bilby_label="bilby_bmk_run"
# The following needs to match 'npool' in the
# config file
ncpus=4

echo "$(date +%c): Activating conda environment ${conda_env}"
conda activate "${conda_env}"
echo "$(date +%c): Running  benchmark"
mkdir "benchmark"; pushd "benchmark" >/dev/null
"${CONDA_PREFIX}/bin/drive_benchmark" --executable ${benchmark_driver} \
    --exec-args ${bilby_label} ${istest} --cpus-per-job ${ncpus} \
    ${splitnodes} ${underutilize} ${localmem} ${verbose} --manager-exec "numactl"
echo "$(date +%c): Parsing results of benchmark"
${results_parser} ${bilby_label}
popd >/dev/null # From 'benchmark'
echo "$(date +%c): Deactivating conda environment ${conda_env}"
conda deactivate

exit 0

