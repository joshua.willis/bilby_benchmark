#!/usr/bin/env python3

# Based on belle2 parseResults.py

import json
import bilby
from bilby.core.result import Result
import argparse
from glob import glob
import re
from os import path, getcwd
from numa_benchmarks.utils import subdir_prefix



result_name="bilby_benchmark_results.json"

parser = argparse.ArgumentParser(
           description="Extract benchmarking data from each result file"
           " in  the subdirectories of a benchmarking run, and collate"
           " the results into the output json file",
           formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument("--subdir-prefix", type=str, default=subdir_prefix,
                    help="Prefix in which to search for directories of"
                    " the form <subdir_prefix>_job<n> for integers n."
                    " Each such directory will be searched for a"
                    " benchmarking output file, with an error if such"
                    " a file is not found. The number after 'job' at"
                    " the end will be used as the job number. All such"
                    " directories in the current working directory will"
                    " be searched.")
parser.add_argument("--bilby-label", type=str, default=None,
                    help="The bilby label of the run in each subdirectory."
                    " Within each subdirectory, the file"
                    " 'result/<bilby_label>_result.json' will be opened"
                    " and parsed to determine the likelihood evaluations"
                    " per second from that job")
parser.add_argument("--result-file", type=str, default=result_name,
                    help="The output JSON file into which to write"
                    " the collated results from each benchmarking"
                    " run. Will be placed in the current directory.")

opts = parser.parse_args()

if opts.bilby_label is None:
    parser.error("You must specify the bilby label of the runs")

basedir = getcwd()
benchmark_dirs = glob(path.join(basedir, opts.subdir_prefix + '_job*'))

if len(benchmark_dirs) == 0:
    raise RuntimeError("No subdirectories of form '{0}_job<n>'"
                       " in the current working directory".format(opts.subdir_prefix))

i = 0
jdict = { "job_number": {}, "likelihood_evals_per_second": {} }
mpattern = '^.+_job(\d+)$'

for wdir in benchmark_dirs:
    m = re.search(mpattern, path.basename(wdir))
    if m is not None:
        jnum = int(m.group(1))
    else:
        raise RuntimeError("Benchmark directory {0} does not end with"
                           " '_job<n>' for some number <n>".format(wdir))
    rfname = path.join(wdir, 'result', opts.bilby_label+'_result.json')
    robj = Result.from_json(rfname)
    nlikelihood = robj.meta_data['run_statistics']['nlikelihood']
    sampling_time = robj.sampling_time.total_seconds()
    del robj

    lps = nlikelihood/sampling_time

    si = str(i)
    jdict["job_number"].update({si: jnum})
    jdict["likelihood_evals_per_second"].update({si:lps})
    i += 1

with open(path.join(basedir, opts.result_file), "w") as OutputFile:
    json.dump(jdict, OutputFile)
