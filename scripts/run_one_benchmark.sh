#!/bin/bash
#
# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

bilby_label=$1
test_flag=$2

# If the second argument is anything at all, we
# are in test mode
if [[ "x${test_flag}" != "x" ]] ; then
    config_file="/bmk/bilby/data/test_config.ini"
    echo "$0: running in test mode"
else
    config_file="/bmk/bilby/data/config_complete.ini"
fi

echo "Checking CPU affinity"
taskset -p $BASHPID

bilby_pipe_analysis ${config_file}  --outdir $PWD --detectors H1 \
      --detectors L1 --label "${bilby_label}" \
      --data-dump-file "/bmk/bilby/data/data_generation.pickle" --sampler dynesty
